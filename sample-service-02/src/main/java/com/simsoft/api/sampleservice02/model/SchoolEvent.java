package com.simsoft.api.sampleservice02.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "school_event")
public class SchoolEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "school_id")
    private int schoolId;

    @Column(name = "name")
    private String name;
}

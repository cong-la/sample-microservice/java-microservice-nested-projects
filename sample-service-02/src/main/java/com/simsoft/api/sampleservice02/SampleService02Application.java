package com.simsoft.api.sampleservice02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleService02Application {

	public static void main(String[] args) {
		SpringApplication.run(SampleService02Application.class, args);
	}

}

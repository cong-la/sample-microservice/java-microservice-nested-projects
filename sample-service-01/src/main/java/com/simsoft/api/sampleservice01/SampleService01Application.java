package com.simsoft.api.sampleservice01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleService01Application {

	public static void main(String[] args) {
		SpringApplication.run(SampleService01Application.class, args);
	}

}

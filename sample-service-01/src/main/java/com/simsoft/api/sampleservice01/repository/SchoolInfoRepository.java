package com.simsoft.api.sampleservice01.repository;

import com.simsoft.api.sampleservice01.model.SchoolInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface SchoolInfoRepository extends JpaRepository<SchoolInfo, Integer> {

    public ArrayList<SchoolInfo> findAll();

    public SchoolInfo findById(int id);

    public SchoolInfo save(SchoolInfo schoolInfo);
}
